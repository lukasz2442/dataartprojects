﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataArt4
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array1 = someSequence(1);
            int[] array2 = someSequence(2);
            maxDifference(array1);
            maxDifference(array2);
            
            Console.ReadKey(); 
		
        }

        static int[] someSequence(int ch)
        {
            Random x = new Random();
            int n = x.Next(10)+6; //array size: from 5 to 15

            int[] array = new int[n];
            for (int i = 0; i < n; i++)
			{
			    array[i]=x.Next(100)+1; //numbers from 1 to 100
			}

            if (ch==1) //ascending
            {
                Array.Sort(array);
            }else
            if (ch==2) //descending
            {
                Array.Sort(array);
                Array.Reverse(array);
            }
            return array;
        }

        static void maxDifference(int[] array)
        {
            foreach (var item in array) Console.Write(item + " "); Console.WriteLine();           

            int max = Math.Abs(array[0] - array[1]), position = 0;
            for (int i = 1; i < array.Length - 1; i++)
            {
                if (Math.Abs(array[i] - array[i + 1]) > max)
                {
                    max = Math.Abs(array[i] - array[i + 1]);
                    position = i;
                }
            }
            Console.WriteLine("Max difference: " + max + ", on position: " + position);
            Console.WriteLine();
        }
    }
}
