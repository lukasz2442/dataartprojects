﻿using FootbalTableGenerator.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballTableGenerator.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Table table = new Table();


            Console.WriteLine("Enter match(es) result(s) with specific entry mode:");
            Console.WriteLine("1) without revenge [HostTeam - GuestTeam GoalsScoredByHostTeam : GoalsScoredByGuestTeam]");
            Console.WriteLine("2) with revenge [Team1 - Team2 GoalsScoredByTeam1AsHost : GoalsScoredByTeam2AsGuest GoalsScoredByTeam2AsHost : GoalsScoredByTeam1AsGuest]");
            while (true)
            {
                string result = Console.ReadLine();
                table.RegisterMatch(result);
                Console.Write(table.Visualize());
                Console.WriteLine();
            }
        }
    }
}
