﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootbalTableGenerator.Core
{
    public class TeamResultsSummarySorter
    {
        private readonly IComparer<TeamResultsSummary> teamComparator = new TeamResultsSummaryComparer();

        public IEnumerable<TeamInTable> Sort(List<TeamResultsSummary> teamsResults)
        {
            teamsResults.Sort(teamComparator);
            teamsResults.Reverse();
            int currentPosition = 1;
            List<TeamInTable> table = new List<TeamInTable>();

            TeamResultsSummary earlierTeamResultsSummary = null;
            foreach (TeamResultsSummary teamResultsSummary in teamsResults)
            {
                if (earlierTeamResultsSummary != null)
                {
                    if (teamComparator.Compare(earlierTeamResultsSummary, teamResultsSummary) != 0)
                    {
                        currentPosition++;
                    }
                }
                earlierTeamResultsSummary = teamResultsSummary;

                TeamInTable teamInTable = new TeamInTable();
                teamInTable.FootballTeamResultsSummary = teamResultsSummary;
                teamInTable.Position = currentPosition;
                table.Add(teamInTable);

                
            }
            return table;
        }
    }
}
