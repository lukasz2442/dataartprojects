﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FootbalTableGenerator.Core
{
    public class MatchBuilder
    {
        private Regex matchStringFormat = new Regex(@"(\w+) - (\w+) (\d+):(\d+)");
        private Regex twoMatchesStringFormat = new Regex(@"(\w+) - (\w+) (\d+):(\d+) (\d+):(\d+)");

        public void ConstructMatch(string matchInput, out FootballMatch fm1, out FootballMatch fm2)
        {
            Match match = matchStringFormat.Match(matchInput);
            Match twoMatch = twoMatchesStringFormat.Match(matchInput);
            fm1 = null;
            fm2 = null;
            if(twoMatch.Success)
            {
                fm1 = new FootballMatch();
                fm1.HostTeam = match.Groups[1].Value;
                fm1.GuestTeam = match.Groups[2].Value;
                fm1.NumberOfGoalsScoredByHosts = Byte.Parse(match.Groups[3].Value);
                fm1.NumberOfGoalsScoredByGuests = Byte.Parse(match.Groups[4].Value);

                fm2 = new FootballMatch();
                fm2.HostTeam = twoMatch.Groups[2].Value;
                fm2.GuestTeam = twoMatch.Groups[1].Value;
                fm2.NumberOfGoalsScoredByHosts = Byte.Parse(twoMatch.Groups[6].Value);
                fm2.NumberOfGoalsScoredByGuests = Byte.Parse(twoMatch.Groups[5].Value);
            }
            else
            if (match.Success)
            {
                fm1 = new FootballMatch();
                fm1.HostTeam = match.Groups[1].Value;
                fm1.GuestTeam = match.Groups[2].Value;
                fm1.NumberOfGoalsScoredByHosts = Byte.Parse(match.Groups[3].Value);
                fm1.NumberOfGoalsScoredByGuests = Byte.Parse(match.Groups[4].Value);
            }
            else
            {
                throw new ArgumentException("Invalid match format");
            }
        }
    }
}
