﻿using FootbalTableGenerator.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballTableGenerator.ConsoleApp2
{
    public class TimeTable
    {
        private string[] teamsNames = null;
        private List<TeamResultsSummary> teamsResults = new List<TeamResultsSummary>();
        private List<FootballMatch> matches = new List<FootballMatch>();

        private readonly MatchRegulations matchRegulations = new MatchRegulations();
        private readonly TableVisualizer tableVisualizer = new TableVisualizer();
        private readonly TeamResultsSummarySorter teamResultsSummarySorter = new TeamResultsSummarySorter();

        public void CreateNew(string teamsString)
        {
            teamsNames = teamsString.Split(' ');
            int numberOfTeams = teamsNames.Length;
            FootballMatch match = null;

            for (int i = 0; i < numberOfTeams-1; i++)
            {
                for (int j = i+1; j < numberOfTeams; j++)
                {
                    //create match
                    match = new FootballMatch();
                    match.HostTeam = teamsNames[i];
                    match.GuestTeam = teamsNames[j];
                    matches.Add(match);

                    //create match revenge
                    match = new FootballMatch();
                    match.HostTeam = teamsNames[j];
                    match.GuestTeam = teamsNames[i];
                    matches.Add(match);
                }
            }

            CreatePseudoResultsForMatches(matches);
        }

        private void CreatePseudoResultsForMatches(List<FootballMatch> matches)
        {
            Random random = new Random();
            foreach (var match in matches)
            {
                match.NumberOfGoalsScoredByHosts = (byte)random.Next(15);
                match.NumberOfGoalsScoredByGuests = (byte)random.Next(15);
                RegisterMatch(match);
            }
        }

        public string VisualizeTimeTable()
        {
            IEnumerable<TeamInTable> teamsEnumeration = teamResultsSummarySorter.Sort(teamsResults);
            return tableVisualizer.Visualize(teamsEnumeration);
        }

        public string VisualizeAllMatches()
        {
            StringBuilder sb = new StringBuilder();
            int licznik = 1;
            foreach (var match in matches)
            {
                sb.AppendLine(String.Format("{0}. {1} - {2} {3}:{4}",
                    licznik++.ToString(),
                    match.HostTeam,
                    match.GuestTeam,
                    match.NumberOfGoalsScoredByHosts,
                    match.NumberOfGoalsScoredByGuests));
            }
            return sb.ToString().Trim();
        }

        private void RegisterMatch(FootballMatch match)
        {
            TeamResultsSummary hostResults = GetTeamResultsByTeamName(match.HostTeam);
            TeamResultsSummary guestResults = GetTeamResultsByTeamName(match.GuestTeam);
            matchRegulations.AddPointsAndGoals(match, hostResults, guestResults);
        }

        private TeamResultsSummary GetTeamResultsByTeamName(string team)
        {
            TeamResultsSummary resultsSummary = teamsResults.Find(tr => tr.Team == team);
            if (resultsSummary == null)
            {
                resultsSummary = new TeamResultsSummary();
                resultsSummary.Team = team;
                teamsResults.Add(resultsSummary);
            };
            return resultsSummary;
        }
    }
}
