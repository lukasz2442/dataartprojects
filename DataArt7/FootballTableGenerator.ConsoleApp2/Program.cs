﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FootballTableGenerator.ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            TimeTable timeTable = new TimeTable();

            Console.WriteLine("Enter teams with space delimiter [Team1 Team2 Team3 Team4...]:");
            while (true)
            {
                timeTable = new TimeTable();
                string result = Console.ReadLine();
                timeTable.CreateNew(result);
                Console.WriteLine("TIMETABLE\n"+timeTable.VisualizeTimeTable());
                Console.WriteLine("ALL MATCHES\n"+timeTable.VisualizeAllMatches());
            }
        }
    }
}
