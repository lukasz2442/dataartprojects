﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataArt3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = { 15, 9, 3, 18, 21, 24 };
            int n = array.Length, result;
            result = NWD(array[0], array[1]);
            for (int i = 2; i < n; i++)
            {
                result = NWD(array[i], result);
            }
            Console.WriteLine(result);
            Console.ReadKey();
        }

        static int NWD(int a, int b)
        {
            int c;
            while(b!=0)
            {
                c = a % b;
                a = b;
                b = c;
            }
            return a;
        }
    }
}
