﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataArt2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the number: ");
            int n = Convert.ToInt32(Console.ReadLine());

            int pierwsza = (int)Math.Sqrt(n);
            int k = 2;

            Console.WriteLine("Prime factors: ");
            while (n > 1 && k <= pierwsza)
            {
                while (n % k == 0)
                {
                    Console.WriteLine(k);
                    n = n / k;
                }
                k++;
            }
            if (n > 1)
            {
                Console.WriteLine(n);
            }
            //comment
            Console.ReadKey();
        }

    }
}