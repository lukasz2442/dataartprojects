﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataArt6
{
    class Program
    {
        /*I have chosen several methods to implement for comfortable use of linked list
         * But first I created helpful private class Node as a represatation of linked element,
         * and that class consists of two elements: stored data and another Note on which implies
         * current Note, then I can access all elements of linked list by invoking next Node of current
         * Node and equal it to current Node until it won't be null
         * 
         * Methods are: 
         * - T ElementAt(int index) - returns element of linked list at specified position
         * - void AddLast(T data) - adds element to the end of the linked list
         * - void AddFirst(T data) - adds element to the beginning of the linked list
         * - bool Add(int index, T data) - inserts element into linked list at specified position
         * - bool RemoveLast() - removes last element of linked list
         * - bool RemoveFirst() - removes first element of linked list
         * - bool RemoveAt(int index) - removes element from linked list at specified position
         * 
         * - property int Count returns number of elements in linked list
         * - method ShowAllNodes() shows all nodes in linked list
         * */
        static void Main(string[] args)
        {
            LinkedListProject<int> list = new LinkedListProject<int>();
            list.AddLast(1);
            list.AddLast(2);
            list.AddLast(3);
            list.AddLast(4);
            list.AddLast(5);
            list.AddLast(6);
            list.AddLast(7);
            list.ShowAllNodes();

            list.AddFirst(10);
            list.Add(5, 100);
            list.ShowAllNodes();
            Console.WriteLine("Element at 4-th position: " + list.ElementAt(4));

            list.RemoveFirst();
            list.RemoveLast();
            list.RemoveAt(4);
            list.ShowAllNodes();

            Console.ReadKey();
        }
    }

    class LinkedListProject<T>
    {
        private Node startNode;
        public int Count
        {
            get
            {
                int licznik = 0;
                Node current = startNode;
                while (current != null)
                {
                    licznik++;
                    current = current.next;
                }
                return licznik;
            }
        }
        
        public T ElementAt(int index)
        {
            Node current = startNode;
            for (int i = 0; i < index; i++) current = current.next;
            return current.data;
        }

        public void ShowAllNodes()
        {
            Node current = startNode;
            while(current!=null)
            {
                Console.Write(current.data+" ");
                current = current.next;
            }
            Console.WriteLine();
        }

        public void AddLast(T data)
        {
            if (startNode == null)
            {
                startNode = new Node();

                startNode.data = data;
                startNode.next = null;
            }
            else
            {
                Node newNode = new Node();
                newNode.data = data;

                Node current = startNode;
                while (current.next != null)
                {
                    current = current.next;
                }

                current.next = newNode;
            }
        }

        public void AddFirst(T data)
        {
            Node newNode = new Node();

            newNode.data = data;
            newNode.next = startNode;

            startNode = newNode;
        }

        public bool Add(int index, T data)
        {
            if (index < 0 || index > this.Count - 1) return false;
            if (index == 0) { this.AddFirst(data); return true; }
            if (index == this.Count) { this.AddLast(data); return true; }

            if (startNode == null)
            {
                startNode = new Node();

                startNode.data = data;
                startNode.next = null;
            }
            else
            {
                Node newNode = new Node();
                newNode.data = data;

                Node current = startNode;

                for (int i = 0; i < index - 1; i++)
                {
                    current = current.next;
                }

                Node tempNode = new Node(current.next.next, current.next.data);
                current.next = newNode;
                newNode.next = tempNode;
            }
            return true;
        }       

        public bool RemoveLast()
        {
            if (startNode == null) return false;
            Node current = startNode;
            for (int i = 0; i < this.Count - 2; i++) current = current.next;            
            current.next = null;
            return true;
        }

        public bool RemoveFirst()
        {
            if (startNode == null) return false;
            startNode = startNode.next;
            return true;
        }

        public bool RemoveAt(int index)
        {
            if (startNode == null || index<0 || index>this.Count-1) return false;
            
            Node current = startNode;
            for (int i = 0; i < index - 1; i++) current = current.next;
            current.next = current.next.next;
            return true;           
        }

        private class Node
        {
            public Node next;
            public T data;

            public Node() { }

            public Node(Node next, T data)
            {
                this.next = next;
                this.data = data;
            }
        }
    }
}
