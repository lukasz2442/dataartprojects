﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataArt1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = { 1, 2, 3, 4, 5 };
            ReverseArray(array);
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }
            Console.ReadKey();
        }

        public static void ReverseArray(int[] array)
        {
            int n = array.Length, pom;
            for (int i = 0; i < n / 2; i++)
            {
                array[i] = array[i] ^ array[n - i - 1];
                array[n - i - 1] = array[i] ^ array[n - i - 1];
                array[i] = array[i] ^ array[n - i - 1];
            }
        }
    }
}