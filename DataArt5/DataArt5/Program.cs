﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataArt5
{
    class Program
    {
        static void Main(string[] args)
        {                      
            string command = "Enter a number: ";
            bool success = false;

            while (!success)
            {
                try
                {
                    Console.Write(command);
                    int n = int.Parse(Console.ReadLine());
                    Console.Write("Result numbers: ");
                    showNumbers(returnRepresentation(n));
                    Console.ReadKey();
                    success = true;
                }
                catch (FormatException)
                {
                    command = "Enter an integer number: ";
                }
            }
  		
        }

        static void showNumbers(List<int> list)
        {
            for (int i = list.Count-1; i > -1; i--)
            {
                Console.Write(list[i]+" ");
            }
            Console.WriteLine();
        }

        static List<int> returnRepresentation(int number)
        {
            if (number == 1 || number == 2 || number == 3) return new List<int> { number };
            int[] array = new int[number];
            List<int> resultList = new List<int>();          
            int a = 1;
            int b = 1;
            int pom, counter=1;
            array[0]=1;
            array[1]=1;

            while(b<=number) 
            {
                pom = b; //1     2     3
                b = a + b; //2   3     5
                a = pom; //1     2     3

                if(b<=number) array[++counter] = b;
            }

            int lastPosition = counter, sum = 0;
            
            while(lastPosition>-1 && sum<number)
            {
                while (lastPosition>-1 && sum + array[lastPosition]>number) lastPosition--;

                sum += array[lastPosition];
                resultList.Add(array[lastPosition]);               
                lastPosition -= 2;
            }

            return resultList;
        }
    }  
}
